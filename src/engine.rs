use std::thread::sleep;
use std::time::Duration;
use crate::graphics::renderer::{Renderer, renderer_initializer};

use crate::graphics::window::WindowOptions;
use crate::AppCreateInfo;
use crate::graphics::input::handeler::{Event, InputHandler};
use crate::scene::RenderObject;
use crate::utils::system::Timer;

pub fn start_engine<A: 'static>(mut application: A, app_context: AppCreateInfo, window_options: WindowOptions) where A: App {
    let config_str = include_str!("../examples/triangle/configs/log.yml"); //TODO: fix hard coded
    let config = serde_yaml::from_str(config_str).unwrap();
    log4rs::init_raw_config(config).unwrap();


    let (mut renderer, event_loop) = renderer_initializer(app_context, window_options);
    let mut input_handler = InputHandler::new(event_loop);

    application.create();
    renderer.init();


    let mut running = true;
    let mut timer = Timer::new();

    while running {
        let delta = timer.delta();
        //info!("FPS: {}", 1_000_000_000 / delta);

        let event = input_handler.read_inputs();

        match event {
            Event::Resize => {
                renderer.resize();
                let (width, height) = renderer.get_dimensions();
                log::debug!("Resize to : {}/{}", width, height);

                application.resize(width, height);
            },
            Event::Close => {
                running = false;
            },
            Event::Continue => ()
        }

        let render_objects = application.update(&mut renderer, delta);
        sleep(Duration::new(0, 2000000));
        renderer.next_frame(render_objects);
    }
}

pub trait App: Sized {
    /// Emitted at the creation of the application.
    fn create(&mut self);
    /// Emitted every frame.
    fn update(&mut self, _renderer: &mut Box<dyn Renderer>, _delta: i128) -> Vec<RenderObject>;
    /// Emitted when the application has been resized.
    fn resize(&mut self, _width: u32, _height: u32);
    /// Emitted when the application has been suspended.
    fn suspend(&mut self);
    /// Emitted when the application has been resumed from suspends.
    fn resumed(&mut self);
    /// Emitted when the application has been asked to quit.
    fn quit(&mut self);
}
#[allow(unused_imports)]
use glium::{glutin, Surface};
use glium::Display;
use glium::index::PrimitiveType;
use winit::dpi::{PhysicalPosition, Position};
use winit::event_loop::EventLoop;

use crate::graphics::renderer::{Renderer, Vertex};
use crate::graphics::window::{WindowMode, WindowOptions};
use crate::AppCreateInfo;
use crate::graphics::camera::Camera;
use crate::scene::{RenderObject};


/// This handles the GLWindow for the OpenGL API
pub struct GLWindow {
    /// The glium target display
    display: Display,
}

impl GLWindow {
    /// Create a new window from the WindowOptions enum
    pub(crate) fn new(opts: WindowOptions) -> (Self, EventLoop<()>) {
        let event_loop = glutin::event_loop::EventLoop::new();
        let wb = glutin::window::WindowBuilder::new()
            //.with_inner_size(Size::Physical(PhysicalSize::new(opts.window_start_width, opts.window_start_height)))
            //.with_min_inner_size(Size::Physical(PhysicalSize::new(opts.window_min_width.unwrap(), opts.window_min_height.unwrap())))
            //.with_max_inner_size(Size::Physical(PhysicalSize::new(opts.window_max_width.unwrap(), opts.window_max_height.unwrap())))
            .with_position(Position::Physical(PhysicalPosition::new(opts.window_start_position_x, opts.window_start_position_y)))
            //.with_resizable(opts.resizable)
            .with_title(opts.window_title)
            .with_maximized(opts.maximized)
            .with_decorations(opts.decorations)
            .with_always_on_top(opts.always_on_top);

        match opts.window_mode {
            WindowMode::Fullscreen => { /* TODO: add fullscreen support here */ }
            WindowMode::BorderlessFullscreen => { /* TODO: add borderless fullscreen support here */ }
            WindowMode::Windowed => {}
        }

        let cb = glutin::ContextBuilder::new(); //TODO: look at these options
        let display = glium::Display::new(wb, cb, &event_loop).unwrap();

        (Self {
            display,
        },
         event_loop)
    }
}

/// OpenGL API controller
pub struct OpenGL {
    /// The target window
    pub window: GLWindow,
    /// The target camera
    pub camera: Camera,
}

impl OpenGL {
    /// Create a new OpenGL API controller
    pub fn new(_app_context: AppCreateInfo, win_opts: WindowOptions) -> (Self, EventLoop<()>) {
        let (window, evenloop) = GLWindow::new(win_opts);

        implement_vertex!(Vertex, position, color);

        (
            Self {
                window,
                camera: Camera::ortho(2., 2.),
            },
            evenloop
        )
    }

    /// Draws stuff on the screen. Run this every time you want to update the screen content.
    fn draw_frame(display: Display, perspectif: [[f32; 4];4 ], objects: Vec<RenderObject>) {
        // compiling shaders and linking them together
        let program = program!(&display,
            430 => {
                vertex: "
                    #version 140
                    uniform mat4 matrix;
                    in vec3 position;
                    in vec3 color;
                    out vec3 vColor;

                    void main() {
                        gl_Position = vec4(position, 1.0);
                        vColor = color;
                    }
                ",

                fragment: "
                    #version 140
                    in vec3 vColor;
                    out vec4 f_color;

                    void main() {
                        f_color = vec4(vColor, 1.0);
                    }
                ",
            },
        ).unwrap();

        // building the uniforms
        let uniforms = uniform! {
                matrix: perspectif
        };

        // drawing a frame
        let mut target_frame = display.draw();
        target_frame.clear_color(0.0, 0.0, 0.0, 0.0);

        for object in objects {
            let vertex_buffer = {
                glium::VertexBuffer::new(&display,
                                         &object.vertices
                ).unwrap()
            };

            // building the index buffer
            let index_buffer = glium::IndexBuffer::new(&display,
                                                       PrimitiveType::TrianglesList,
                                                       &object.indices).unwrap();

            target_frame.draw(&vertex_buffer, &index_buffer, &program, &uniforms, &Default::default()).unwrap();
        }

        target_frame.finish().unwrap();
    }
}

impl Renderer for OpenGL {
    /// Init OpenGL API
    fn init(&mut self) { }

    /// Run this if the window has been resized
    fn resize(&mut self) { }

    /// Run this if you want to display the next frame
    fn next_frame(&mut self, render_objects: Vec<RenderObject>) {
        let perspectif: [[f32; 4];4 ] = self.camera.combined().into();

        OpenGL::draw_frame(self.window.display.clone(), perspectif, render_objects);
    }

    /// Cleans up the OpenGL memory. Run this before you quite the program.
    fn cleanup(&mut self) {
        debug!("Starting to clean up");
    }

    /// Gets the camera
    fn get_camera(&mut self) -> &mut Camera {
        &mut self.camera
    }

    /// Gets the window dimensions
    fn get_dimensions(&mut self) -> (u32, u32) {
        (0, 0) //TODO get the dimensions
    }

    /// Gets which API this is
    fn get_api(&mut self) -> String {
        "OpenGL".parse().unwrap()
    }
}

use std::fmt;
use std::fmt::Formatter;
use winit::event_loop::EventLoop;

#[cfg(feature = "opengl")]
use crate::graphics::opengl::api::OpenGL;
use crate::graphics::window::WindowOptions;
use crate::AppCreateInfo;
use crate::graphics::camera::Camera;
use crate::scene::RenderObject;

/// A point in space with a color
#[derive(Copy, Clone)]
pub struct Vertex {
    pub position: [f32; 3],
    pub color: [f32; 3],
}

/// All the render APIs
#[derive(Debug)]
pub enum RenderAPI {
    Vulkan,
    OpenGL,
    Metal,
}

impl fmt::Display for RenderAPI {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

/// Initialise the correct render API
#[allow(unused_variables)]
pub fn renderer_initializer(app_context: AppCreateInfo, window_options: WindowOptions) -> (Box<dyn Renderer>, EventLoop<()>) {
    match app_context.graphics_api {
        //RenderAPI::Vulkan => { return Box::new(Vulkan::new(app_context)) }
        #[cfg(feature = "opengl")]
        RenderAPI::OpenGL => {
            let (api, eventloop) = OpenGL::new(app_context, window_options);

            (Box::new(api), eventloop)
        }
        _ => {
            panic!("Didn't {} api isn't implemented, check if the correct features are enabled.", app_context.graphics_api)
        }
    }
}

/// Boilerplate for the render APIs
pub trait Renderer {
    fn init(&mut self);
    fn resize(&mut self);
    fn next_frame(&mut self, game_objects: Vec<RenderObject>);
    fn cleanup(&mut self);
    fn get_camera(&mut self) -> &mut Camera;
    /// Returns the dimensions of the window in width, height.
    fn get_dimensions(&mut self) -> (u32, u32);
    fn get_api(&mut self) -> String;
}

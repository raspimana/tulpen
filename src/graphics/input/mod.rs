pub mod handeler;

// All keyboard keys
#[derive(Copy, Clone)]
pub enum KeyKeyboard {
    Left = 0,
    Right,
    Up,
    Down,
    PageUp,
    PageDown,
    Return,
    Space,
    Tab,
    A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z,
    Unknown
}

// Used to write the keyboard event info to
#[derive(Copy, Clone)]
pub struct InputMap {
    state: [bool; 1 + KeyKeyboard::Unknown as usize],
    previous: [bool; 1 + KeyKeyboard::Unknown as usize]
}

impl InputMap {
    pub fn new() -> Self {
        InputMap {
            state: [false; 1 + KeyKeyboard::Unknown as usize],
            previous: [false; 1 + KeyKeyboard::Unknown as usize]
        }
    }

    pub fn key_press(&mut self, key: KeyKeyboard) {
        self.state[key as usize] = true;
    }

    pub fn key_release(&mut self, key: KeyKeyboard) {
        self.state[key as usize] = false;
    }

    pub fn pressed(&self, key: KeyKeyboard) -> bool {
        self.state[key as usize] && !self.previous[key as usize]
    }

    pub fn reset(&mut self) {
        self.previous = self.state;
        self.state = [false; 1 + KeyKeyboard::Unknown as usize];
    }
}

impl Default for InputMap {
    fn default() -> Self {
        Self::new()
    }
}
use std::sync::Arc;
use std::default::Default;

use vulkano::device::{Device, DeviceCreateInfo, Queue, Features, DeviceExtensions};
use vulkano::device::physical::PhysicalDevice;
use vulkano::instance::debug::{DebugCallback, MessageSeverity, MessageType};

use vulkano::instance::{Instance, InstanceExtensions, InstanceCreateInfo, layers_list};
use vulkano::Version;
use vulkano::image::{ImageUsage, SwapchainImage};
use vulkano::swapchain::{ColorSpace, FullScreenExclusive, PresentMode, Surface, SurfaceCapabilities, Swapchain, SwapchainCreateInfo};
use vulkano::sync::Sharing;
use winit::event_loop::EventLoop;

use winit::window::{Window, WindowBuilder};
use crate::{ENGINE_NAME, ENGINE_VERGION};

use crate::{AppCreateInfo, Version as MyVersion};
use super::assist::QueueFamilyIndices;


const WIDTH: u32 = 800;
const HEIGHT: u32 = 600;
const VALIDATION_LAYERS: &[&str] = &[ "VK_LAYER_LUNARG_standard_validation" ];

#[cfg(all(debug_assertions))]
const ENABLE_VALIDATION_LAYERS: bool = true;
#[cfg(not(debug_assertions))]
const ENABLE_VALIDATION_LAYERS: bool = false;

/// Required device extensions
fn device_extensions() -> DeviceExtensions {
    DeviceExtensions {
        khr_swapchain: true,
        .. vulkano::device::DeviceExtensions::none()
    }
}

const fn version_to_vkversion(version: MyVersion) -> Version {
    Version {
        major: version.major,
        minor: version.minor,
        patch: version.patch
    }
}


pub struct Vulkan {
    debug_callback: Option<DebugCallback>,
    surface: Arc<Surface<Window>>,

    instance: Option<Arc<Instance>>,
    physical_device_index: usize, // can't store PhysicalDevice directly (lifetime issues)
    device: Arc<Device>,

    graphics_queue: Arc<Queue>,
    present_queue: Arc<Queue>,

    swap_chain: Arc<Swapchain<Window>>,
    swap_chain_images: Vec<Arc<SwapchainImage<Window>>>,
}

impl Vulkan {
    pub fn new(app_context: AppCreateInfo) -> Self {
        let instance: Arc<Instance> = Self::create_instance(
            app_context.app_name,
            version_to_vkversion(app_context.app_version),
        );
        let debug_callback = Self::setup_debug_callback(&instance);

        //TODO: FIX THIS THIS IS CRUTIAL
        let window = WindowBuilder::new().build(&EventLoop::new()).unwrap();
        let surface = Self::create_surface(&instance, /*app_context.window*/window);

        let physical_device_index = Self::pick_physical_device(&instance, &surface);
        let (device, graphics_queue, present_queue) = Self::create_logical_device(
            &instance, &surface, physical_device_index);
        let (swap_chain, swap_chain_images) = Self::create_swap_chain(&instance, &surface, physical_device_index,
                                                                      &device, &graphics_queue, &present_queue);

        Self {
            instance: Some(instance),
            debug_callback,
            physical_device_index,
            device,
            graphics_queue,
            surface,
            present_queue,
            swap_chain,
            swap_chain_images,
        }
    }

    fn create_surface(instance: &Arc<Instance>, window: Window) -> Arc<Surface<Window>> {
        vulkano_win::create_surface_from_winit(window, instance.clone()).unwrap()
    }

    fn create_logical_device(
        instance: &Arc<Instance>,
        surface: &Arc<Surface<Window>>,
        physical_device_index: usize,
    ) -> (Arc<Device>, Arc<Queue>, Arc<Queue>) {
        let physical_device = PhysicalDevice::from_index(instance, physical_device_index).unwrap();
        let _indices = Self::find_queue_families(surface, &physical_device);

        // NOTE: the tutorial recommends passing the validation layers as well
        // for legacy reasons (if ENABLE_VALIDATION_LAYERS is true). Vulkano handles that
        // for us internally.

        let creation_info = DeviceCreateInfo {
            enabled_extensions: DeviceExtensions::none(), //TODO: enable some extensions
            enabled_features: Features::none(),
            queue_create_infos: Vec::new(), //TODO: look at this and put the correct info in it
            _ne: Default::default()
        };

        let (device, mut queues) = Device::new(physical_device, creation_info)
            .expect("failed to create logical device!");

        let graphics_queue = queues.next().unwrap();
        let present_queue = queues.next().unwrap_or_else(|| graphics_queue.clone());

        (device, graphics_queue, present_queue)
    }

    fn pick_physical_device(instance: &Arc<Instance>, surface: &Arc<Surface<Window>>) -> usize {
        PhysicalDevice::enumerate(instance)
            .position(|device| Self::is_device_suitable(surface, &device))
            .expect("failed to find a suitable GPU!")
    }

    fn is_device_suitable(surface: &Arc<Surface<Window>>, device: &PhysicalDevice) -> bool {
        let indices = Self::find_queue_families(surface, device);
        let extensions_supported = Self::check_device_extension_support(device);

        let swap_chain_adequate = false; //TODO look at the capabilities of the surface

        indices.is_complete() && extensions_supported && swap_chain_adequate
    }

    fn check_device_extension_support(device: &PhysicalDevice) -> bool {
        let available_extensions = DeviceExtensions::supported_by_device(*device);
        let device_extensions = device_extensions();
        available_extensions.intersection(&device_extensions) == device_extensions
    }

    fn choose_swap_present_mode(/*available_present_modes: SupportedPresentModes*/) -> PresentMode {
        // TODO: this should be look at when optimizing, this is how to swapchain handels multiple
        // images: SupportedPresentModes is not supported anymore so I need to find an alternative
        // to find out what kind of present mode the screen supports.
        /*if available_present_modes.mailbox {
            PresentMode::Mailbox
        } else if available_present_modes.immediate {
            PresentMode::Immediate
        } else {
            PresentMode::Fifo
        }
        */

        PresentMode::Fifo
    }

    fn choose_swap_extent(capabilities: &SurfaceCapabilities) -> [u32; 2] {
        if let Some(current_extent) = capabilities.current_extent {
            current_extent
        } else {
            let mut actual_extent = [WIDTH, HEIGHT];
            actual_extent[0] = capabilities.min_image_extent[0]
                .max(capabilities.max_image_extent[0].min(actual_extent[0]));
            actual_extent[1] = capabilities.min_image_extent[1]
                .max(capabilities.max_image_extent[1].min(actual_extent[1]));
            actual_extent
        }
    }

    fn create_swap_chain(
        instance: &Arc<Instance>,
        surface: &Arc<Surface<Window>>,
        physical_device_index: usize,
        device: &Arc<Device>,
        _graphics_queue: &Arc<Queue>,
        _present_queue: &Arc<Queue>,
    ) -> (Arc<Swapchain<Window>>, Vec<Arc<SwapchainImage<Window>>>) {
        let physical_device = PhysicalDevice::from_index(instance, physical_device_index).unwrap();

        let capabilities = physical_device
            .surface_capabilities(surface, Default::default())
            .unwrap();

        let present_mode = Self::choose_swap_present_mode();
        let extent = Self::choose_swap_extent(&capabilities);

        let mut image_count = capabilities.min_image_count + 1;
        if capabilities.max_image_count.is_some() && image_count > capabilities.max_image_count.unwrap() {
            image_count = capabilities.max_image_count.unwrap();
        }

        let image_usage = ImageUsage::color_attachment();
        // Choosing the internal format that the images will have.
        let image_format = Some(
            physical_device
                .surface_formats(surface, Default::default())
                .unwrap()[0]
                .0,
        );

        let composite_alpha = capabilities
            .supported_composite_alpha
            .iter()
            .next()
            .unwrap();

        // TODO: this should be looked at when optimizing
        let creation_info = SwapchainCreateInfo {
            min_image_count: image_count,
            image_format,
            image_color_space: ColorSpace::SrgbNonLinear,
            image_extent: extent,
            image_array_layers: 1,
            image_usage,
            image_sharing: Sharing::Exclusive,
            pre_transform: capabilities.current_transform,
            composite_alpha,
            present_mode,
            clipped: true,
            full_screen_exclusive: FullScreenExclusive::Default, //TODO change fullscreen exclusive
            win32_monitor: None,

            ..Default::default()
        };

        let (swap_chain, images) = Swapchain::new(
            device.clone(),
            surface.clone(),
            creation_info,
        ).expect("failed to create swap chain!");

        (swap_chain, images)
    }

    fn find_queue_families(_surface: &Arc<Surface<Window>>, device: &PhysicalDevice) -> QueueFamilyIndices {
        let mut indices = QueueFamilyIndices::new();
        // TODO: replace index with id to simplify?
        for (i, queue_family) in device.queue_families().enumerate() {
            if queue_family.supports_graphics() {
                indices.graphics_family = i as i32;
            }

            if indices.is_complete() {
                break;
            }
        }

        indices
    }

    /// Create an instance with instance creation info. The instance creation instance includes the name and version from the engine and a validation layer.
    ///
    /// # Arguments
    ///
    /// * `application_name` - The name of the app you are making
    /// * `application_version` - The version of the app you are making
    /// * `validation_layers` - layers that needs to be enabled
    ///
    /// # Return
    ///
    /// The instance is the connection between your application and the Vulkan library and creating it involves specifying some details about your application to the driver.
    ///
    fn create_instance(application_name: String, application_version: Version) -> Arc<Instance> {
        if ENABLE_VALIDATION_LAYERS && !Self::check_validation_layer_support() {
            debug!("Validation layers requested, but not available!")
        }

        let supported_extensions = InstanceExtensions::supported_by_core()
            .expect("failed to retrieve supported extensions");
        debug!("Supported extensions: {:?}", supported_extensions);

        let app_info = InstanceCreateInfo {
            application_name: Some(application_name),
            application_version,
            //TODO: add engine version and name as const in some file to implement here
            engine_name: Some(ENGINE_NAME.parse().unwrap()),
            engine_version: version_to_vkversion(ENGINE_VERGION),
            enabled_extensions: vulkano_win::required_extensions(),
            enabled_layers: {
                if ENABLE_VALIDATION_LAYERS && Self::check_validation_layer_support() {
                    Vec::new() //TODO pass VALIDATION_LAYERS here
                } else {
                    Vec::new()
                }
            },
            ..Default::default()
        };

        Instance::new(app_info)
            .expect("failed to create Vulkan instance")
    }

    fn get_required_extensions() -> InstanceExtensions {
        let mut extensions = vulkano_win::required_extensions();
        if ENABLE_VALIDATION_LAYERS {
            extensions.ext_debug_report = true;
        }

        extensions
    }

    fn check_validation_layer_support() -> bool {
        let layers: Vec<_> = layers_list().unwrap().map(|l| l.name().to_owned()).collect();
        VALIDATION_LAYERS.iter()
            .all(|layer_name| layers.contains(&layer_name.to_string()))
    }

    fn setup_debug_callback(instance: &Arc<Instance>) -> Option<DebugCallback> {
        if !ENABLE_VALIDATION_LAYERS  {
            return None;
        }

        //TODO: beter config message types
        let msg_type = MessageType {
            general: true,
            validation: false,
            performance: false
        };
        let msg_severity = MessageSeverity {
            error: true,
            warning: true,
            information: false,
            verbose: false
        };
        DebugCallback::new(instance, msg_severity, msg_type, |msg| {
            debug!("validation layer: {:?}", msg.description);
        }).ok()
    }
}

pub(crate) struct QueueFamilyIndices {
    pub(crate) graphics_family: i32,
    pub(crate) present_family: i32,
}
impl QueueFamilyIndices {
    pub(crate) fn new() -> Self {
        Self {
            graphics_family: -1,
            present_family: -1
        }
    }

    pub(crate) fn is_complete(&self) -> bool {
        self.graphics_family >= 0 && self.present_family >= 0
    }
}
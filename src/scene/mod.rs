
use crate::graphics::renderer::Vertex;

pub mod shapes;

pub trait Object {
    // Get the vertexes to pass to the render API
    fn get_render_object(&self) -> RenderObject;
}

struct ObjectManager {
    objects: [Box<dyn Object>]
}

impl ObjectManager {

}

pub struct RenderObject {
    pub vertices: Vec<Vertex>,
    pub indices: Vec<u32>,
}
# Tulpen
[![pipeline status](https://gitlab.com/tulpen/tulpen/badges/main/pipeline.svg)](https://gitlab.com/tulpen/tulpen/commits/master)
[![coverage report](https://user-content.gitlab-static.net/44cf09132f293ec5f06770a8778c4bf02029c3cd/68747470733a2f2f696d672e736869656c64732e696f2f6769746875622f6c6963656e73652f76656c6f72656e2f76656c6f72656e2e737667)](https://gitlab.com/tulpen/tulpen/commits/master)
[![dependency status](https://deps.rs/repo/gitlab/tulpen/tulpen/status.svg)](https://deps.rs/repo/gitlab/tulpen/tulpen)
[![lines of code](https://tokei.rs/b1/gitlab/tulpen/tulpen)](https://tokei.rs/b1/gitlab/tulpen/tulpen)

A game engine written with Vulkano (= Vulkan library for rust) and Rust.

## features
- [x] 2D OpenGL support
- [ ] basic input support
- [ ] multiple camera support
- [ ] Add documentation to camera
 
## Installation
You can install the project with rust

```shell
git clone https://gitlab.com/livy_y/rust_game_engine
cd rust_game_engine

cargo build
```

to run examples:
```shell
# this list all the examples
cargo run --example 
# this runs the example example_name
cargo run --example example_name
```

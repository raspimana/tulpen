use cgmath::{Angle, Deg, Point3};
use log::{debug, info};
use tulpen::{AppCreateInfo, engine, graphics::renderer::{RenderAPI}, Version};
use tulpen::graphics::renderer::{Renderer, Vertex};
use tulpen::graphics::window::WindowOptions;
use tulpen::scene::RenderObject;
use tulpen::scene::shapes::{ShapeBuilder, Shapes2D};

struct Game {
    rotation: u64,
}

impl engine::App for Game {
    fn create(&mut self) { debug!("created the game") }

    fn update(&mut self, renderer: &mut Box<dyn Renderer>, _delta: i128) -> Vec<RenderObject> {
        renderer.get_camera().set_position(Point3::new(0.0, 10.0, 0.0));

        self.rotation += 1;

        let vertices =  [
            Vertex { position: [0.0,  0.0, 0.], color: [1.0, 0.0, 0.0] },
            Vertex { position: [-Angle::cos(Deg(self.rotation as f32))-Angle::sin(Deg(self.rotation as f32)),
                Angle::cos(Deg(self.rotation as f32))-Angle::sin(Deg(self.rotation as f32)), 0.], color: [0.0, 1.0, 0.0] },
            Vertex { position: [-Angle::cos(Deg((self.rotation+90) as f32))-Angle::sin(Deg((self.rotation+90) as f32)),
                Angle::cos(Deg((self.rotation+90) as f32))-Angle::sin(Deg((self.rotation+90) as f32)), 0.], color: [0.0, 0.0, 1.0] },
        ];

        let rotating_triangle = RenderObject {
            vertices: Vec::from(vertices),
            indices: Vec::from([0u32, 1, 2])
        };

        let triangle = ShapeBuilder::new()
            .height(100_f32)
            .width(200_f32)
            .color([1.0, 1.0, 1.0])
            .build();


        println!("test {}", triangle.vertices[0].color[0]);

        let h_line = RenderObject{
            vertices: Vec::from([
                Vertex { position: [ 0.0000,  5.0, 0.], color: [1.0, 0.0, 0.0] },
                Vertex { position: [-0.0005, -5.0, 0.], color: [1.0, 0.0, 0.0] },
                Vertex { position: [ 0.0005, -5.0, 0.], color: [1.0, 0.0, 0.0] },
            ]),
            indices: vec![1,2,3]
        };

        let v_line = RenderObject{
            vertices: Vec::from([
                Vertex { position: [ 5.0,  0.000, 0.], color: [1.0, 0.0, 0.0] },
                Vertex { position: [-5.0, -0.004, 0.], color: [1.0, 0.0, 0.0] },
                Vertex { position: [-5.0,  0.004, 0.], color: [1.0, 0.0, 0.0] },
            ]),
            indices: vec![1,2,3]
        };

        Vec::from([h_line, triangle, rotating_triangle, v_line])
    }

    fn resize(&mut self, _width: u32, _height: u32) { debug!("resized the window") }

    fn suspend(&mut self) {
        debug!("suspend the game")
    }

    fn resumed(&mut self) {
        debug!("resume the game")
    }

    fn quit(&mut self) {
        debug!("quit the game")
    }
}

impl Game {
    pub fn new() -> Game {
        Game{ rotation: 0}
    }
}

fn main() {
    info!("Starting game!");

    let window_options = WindowOptions::default();

    let creation_info = AppCreateInfo {
        app_name: "Triangle game".parse().unwrap(),
        app_version: Version::default(),
        graphics_api: RenderAPI::OpenGL,
    };

    let game = Game::new();
    engine::start_engine(game, creation_info, window_options);

    info!("Quited game!");
}
